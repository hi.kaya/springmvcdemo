package com.luv2code.springdemo;

import java.util.LinkedHashMap;

public class Student {
	
	private String firstName;
	private String secondName;
	private String country;
	
	private LinkedHashMap<String,String> countryOptions;
	
	public Student() {
		countryOptions = new LinkedHashMap<>();
		
		countryOptions.put("BR","Brazil");
		countryOptions.put("IN","INDIA");
		countryOptions.put("FR","FRANCE");
		countryOptions.put("DE","GERMANY");
		countryOptions.put("US","United States Of America");


	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public LinkedHashMap<String, String> getCountryOptions() {
		return countryOptions;
	}

	
	
	
	
}
